# react-native-fullyverifiedsdk

## Getting started

`$ npm install react-native-fullyverifiedsdk --save`

### Mostly automatic installation

`$ react-native link react-native-fullyverifiedsdk`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-fullyverifiedsdk` and add `Fullyverifiedsdk.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libFullyverifiedsdk.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.reactlibrary.FullyverifiedsdkPackage;` to the imports at the top of the file
  - Add `new FullyverifiedsdkPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-fullyverifiedsdk'
  	project(':react-native-fullyverifiedsdk').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-fullyverifiedsdk/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-fullyverifiedsdk')
  	```


## Usage
```javascript
import Fullyverifiedsdk from 'react-native-fullyverifiedsdk';

// TODO: What to do with the module?
Fullyverifiedsdk;
```
