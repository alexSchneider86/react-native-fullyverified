import { NativeModules } from 'react-native';

const { FullyverifiedSDK } = NativeModules;

export default FullyverifiedSDK;
